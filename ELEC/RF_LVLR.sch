EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5500 3900 5350 3900
Connection ~ 5350 3900
Wire Wire Line
	5350 3900 5100 3900
$Comp
L pspice:R R303
U 1 1 5F34741A
P 5750 3900
F 0 "R303" V 5955 3900 50  0000 C CNN
F 1 "2.4k" V 5864 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5750 3900 50  0001 C CNN
F 3 "~" H 5750 3900 50  0001 C CNN
	1    5750 3900
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C302
U 1 1 5F347431
P 6000 2800
F 0 "C302" V 5685 2800 50  0000 C CNN
F 1 "100n" V 5776 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6000 2800 50  0001 C CNN
F 3 "~" H 6000 2800 50  0001 C CNN
	1    6000 2800
	-1   0    0    1   
$EndComp
Text Notes 4300 4400 0    50   ~ 0
DC_BIAS
Wire Wire Line
	6250 3150 6000 3150
Connection ~ 6000 3150
Wire Wire Line
	6000 3150 6000 3050
Wire Wire Line
	5350 3800 5350 3900
Wire Wire Line
	4900 2800 4900 2850
Wire Wire Line
	5350 3300 5350 2850
$Comp
L pspice:R R302
U 1 1 5F347446
P 5350 3550
F 0 "R302" V 5555 3550 50  0000 C CNN
F 1 "47k" V 5464 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5350 3550 50  0001 C CNN
F 3 "~" H 5350 3550 50  0001 C CNN
	1    5350 3550
	-1   0    0    1   
$EndComp
$Comp
L pspice:CAP C301
U 1 1 5F34744C
P 4900 2550
F 0 "C301" H 4722 2504 50  0000 R CNN
F 1 "1000p" H 4722 2595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4900 2550 50  0001 C CNN
F 3 "~" H 4900 2550 50  0001 C CNN
	1    4900 2550
	-1   0    0    1   
$EndComp
$Comp
L Transistor_BJT:BFR92 Q301
U 1 1 5F347457
P 4900 3800
F 0 "Q301" V 5135 3800 50  0000 C CNN
F 1 "BFP420" V 5226 3800 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-343_SC-70-4" H 5100 3725 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BFR92A_N.pdf" H 4900 3800 50  0001 L CNN
F 4 "Q" H 4900 3800 50  0001 C CNN "Spice_Primitive"
F 5 "BFP420" H 4900 3800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4900 3800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "KCSL\\BFP420.lib" H 4900 3800 50  0001 C CNN "Spice_Lib_File"
	1    4900 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2850 4900 2850
Wire Wire Line
	4900 2850 4900 3000
Connection ~ 4900 2850
Text Label 6250 3150 0    50   ~ 0
+V
Text Label 7200 3050 0    50   ~ 0
+V
Text Label 7500 3050 0    50   ~ 0
RF_LVL_OUT
Wire Wire Line
	7200 3050 7500 3050
Text Label 7200 3200 0    50   ~ 0
+V
Text HLabel 7650 3200 2    50   Input ~ 0
+V
Wire Wire Line
	6000 3150 6000 3900
Wire Wire Line
	7650 3200 7200 3200
Text Label 7200 3350 0    50   ~ 0
RF_IN
Text HLabel 7650 3350 2    50   Input ~ 0
RF_IN
Wire Wire Line
	7650 3350 7200 3350
Text Label 7200 3500 0    50   ~ 0
RF_OUT
Text HLabel 7650 3500 2    50   Output ~ 0
RF_OUT
Wire Wire Line
	7200 3500 7650 3500
Text Label 4900 1850 3    50   ~ 0
RF_IN
Text Label 5350 4350 1    50   ~ 0
RF_OUT
Wire Wire Line
	5350 4350 5350 3900
Wire Wire Line
	4900 1850 4900 2300
Text Label 7200 3650 0    50   ~ 0
GND
Text HLabel 7650 3650 2    50   Input ~ 0
GND
Wire Wire Line
	7200 3650 7650 3650
Text Label 4250 3900 0    50   ~ 0
GND
Wire Wire Line
	4250 3900 4700 3900
Text Label 6000 2150 3    50   ~ 0
GND
Wire Wire Line
	6000 2150 6000 2550
$Comp
L pspice:R R301
U 1 1 5F3CF8A0
P 4900 3250
F 0 "R301" V 5105 3250 50  0000 C CNN
F 1 "50" V 5014 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4900 3250 50  0001 C CNN
F 3 "~" H 4900 3250 50  0001 C CNN
	1    4900 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 3500 4900 3600
$EndSCHEMATC
