EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 8650 2850 0    50   ~ 0
+V
Text Label 8950 2850 0    50   ~ 0
RF_LVL_OUT
Wire Wire Line
	8650 2850 8950 2850
Text Label 8650 3000 0    50   ~ 0
+V
Text HLabel 9100 3000 2    50   Input ~ 0
+V
Wire Wire Line
	9100 3000 8650 3000
Text Label 8650 3150 0    50   ~ 0
RF_IN
Text HLabel 9100 3150 2    50   Input ~ 0
RF_IN
Wire Wire Line
	9100 3150 8650 3150
Text Label 8650 3300 0    50   ~ 0
RF_OUT
Text HLabel 9100 3300 2    50   Output ~ 0
RF_OUT
Wire Wire Line
	8650 3300 9100 3300
Text Label 8650 3450 0    50   ~ 0
GND
Text HLabel 9100 3450 2    50   Input ~ 0
GND
Wire Wire Line
	8650 3450 9100 3450
Wire Wire Line
	5450 2950 5500 2950
Wire Wire Line
	7100 2950 7300 2950
Connection ~ 7100 2950
$Comp
L pspice:CAP C?
U 1 1 5F3EDBC4
P 7100 3300
AR Path="/5F3EDBC4" Ref="C?"  Part="1" 
AR Path="/5F33D501/5F3EDBC4" Ref="C201"  Part="1" 
F 0 "C201" V 6785 3300 50  0000 C CNN
F 1 "100n" V 6876 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7100 3300 50  0001 C CNN
F 3 "~" H 7100 3300 50  0001 C CNN
	1    7100 3300
	-1   0    0    1   
$EndComp
Text Label 7100 4000 1    50   ~ 0
GND
Wire Wire Line
	7100 3050 7100 2950
Text Label 7300 2950 2    50   ~ 0
+V
Text Label 4150 2950 2    50   ~ 0
GND
Text Label 5450 2000 3    50   ~ 0
RF_OUT
$Comp
L Device:Q_NJFET_SGD Q201
U 1 1 5F418108
P 4500 4450
F 0 "Q201" V 4735 4450 50  0000 C CNN
F 1 "Q_NJFET_SGD" V 4826 4450 50  0000 C CNN
F 2 "" H 4700 4550 50  0001 C CNN
F 3 "~" H 4500 4450 50  0001 C CNN
F 4 "X" H 4500 4450 50  0001 C CNN "Spice_Primitive"
F 5 "CFY30" H 4500 4450 50  0001 C CNN "Spice_Model"
F 6 "N" H 4500 4450 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "KCSL\\RF_SILICON.lib" H 4500 4450 50  0001 C CNN "Spice_Lib_File"
	1    4500 4450
	0    -1   -1   0   
$EndComp
Text Label 5250 2150 3    50   ~ 0
RF_IN
Wire Wire Line
	6550 2950 7100 2950
Wire Wire Line
	6000 2950 6050 2950
$Comp
L pspice:INDUCTOR L?
U 1 1 5F3ECB69
P 6300 2950
AR Path="/5F3ECB69" Ref="L?"  Part="1" 
AR Path="/5F33D501/5F3ECB69" Ref="L201"  Part="1" 
F 0 "L201" H 6300 2769 50  0000 C CNN
F 1 "1000n" H 6300 2860 50  0000 C CNN
F 2 "" H 6300 2950 50  0001 C CNN
F 3 "~" H 6300 2950 50  0001 C CNN
	1    6300 2950
	-1   0    0    1   
$EndComp
$Comp
L pspice:R R?
U 1 1 5F409F71
P 5750 2950
AR Path="/5F409F71" Ref="R?"  Part="1" 
AR Path="/5F33D501/5F409F71" Ref="R202"  Part="1" 
F 0 "R202" V 5955 2950 50  0000 C CNN
F 1 "10k" V 5864 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5750 2950 50  0001 C CNN
F 3 "~" H 5750 2950 50  0001 C CNN
	1    5750 2950
	0    1    1    0   
$EndComp
Connection ~ 5450 2950
Wire Wire Line
	7100 3550 7100 4000
Wire Wire Line
	4150 2950 5050 2950
Wire Wire Line
	5450 2000 5450 2950
$Comp
L Device:Q_NJFET_SDG Q?
U 1 1 5F35D5D5
P 5250 2850
AR Path="/5F35D5D5" Ref="Q?"  Part="1" 
AR Path="/5F33D501/5F35D5D5" Ref="Q202"  Part="1" 
F 0 "Q202" V 5485 2850 50  0000 C CNN
F 1 "J309" V 5576 2850 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 2950 50  0001 C CNN
F 3 "~" H 5250 2850 50  0001 C CNN
F 4 "J" H 5250 2850 50  0001 C CNN "Spice_Primitive"
F 5 "Q_NJFET_DGS" H 5250 2850 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5250 2850 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "KCSL\\J309_ngspice.lib" H 5250 2850 50  0001 C CNN "Spice_Lib_File"
	1    5250 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 2150 5250 2650
$EndSCHEMATC
